#include <stdio.h>
#include <stdlib.h>

int a[100];

void swap(int*, int*);

void invertOfRecursive(int *, int, int);
void iteration(int *, int, int);
int main() {
	int index, result;
	for (index = 0; index < 10; index++)
		a[index] = index;
	iteration(a, 0, 9);
	for (result = 0; result < 10; result++)
		printf("%d", a[result]);
	return 0;
}
void swap(int *left, int *right) {
	int middle;
	middle = *left;
	*left = *right;
	*right = middle;
}

/*
 * recursive
 */
void invertOfRecursive(int *recursive, int low, int high) {
	if (low < high) {
		swap(&recursive[low], &recursive[high]);
		invertOfRecursive(recursive, low + 1, high - 1);
	}
}
/*
 * iteration
 */
void iteration(int *iteration, int low, int high) {
	while (low < high) {
		swap(&iteration[low++], &iteration[high--]);
	}
}
