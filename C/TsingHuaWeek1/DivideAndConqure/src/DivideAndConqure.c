/*
 ============================================================================
 Name        : DivideAndConqure.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
int a[100];
int sumOfArr(int *, int, int);

int main(void) {
	int index, out;
	for (index = 0; index < 10; index++)
		a[index] = index;

	printf("%d\n", sumOfArr(a, 0, 9));

	for (out = 0; out < 10; out++)
		printf("%d", a[out]);
	return 0;
}
int sumOfArr(int *divide, int min, int max) {
	if (min == max) {
		return divide[min];
	} else {
		int mid = (min + max) >> 1;
		return sumOfArr(divide, min, mid) + sumOfArr(divide, mid + 1, max);
	}
}
