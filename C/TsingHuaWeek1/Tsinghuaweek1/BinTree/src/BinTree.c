/*
 ============================================================================
 Name        : BinTree.c
 Author      : meank
 Version     : 1
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
typedef struct _Tree Tree;
struct _Tree {
	int value;
	Tree *left;
	Tree *right;
};
int main(void) {

	return 0;
}
/*
 * ǰ��
 */
void PreOrderTraversal(Tree *tree) {
	if (tree) {
		printf("%d ", tree->value);
		PreOrderTraversal(tree->left);
		PreOrderTraversal(tree->right);
	}
}
/*
 * ����
 */
void PreOrderTraversal1(Tree *tree) {
	if (tree) {
		PreOrderTraversal(tree->left);
		printf("%d ", tree->value);
		PreOrderTraversal(tree->right);
	}
}
/*
 * ����
 */
void PreOrderTraversal2(Tree *tree) {
	if (tree) {
		PreOrderTraversal(tree->left);
		PreOrderTraversal(tree->right);
		printf("%d ", tree->value);
	}
}
