/*
 ============================================================================
 Name        : MAXtwo.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
int a[100];

void findMaxTwo(int*, int, int)
void swap(int*, int*);

int main() {
	int index = 0;

	for (index = 0; index < 50; index++) {
		a[index] = index;
	}
	findMaxTwo(a, 0, 49);
	return 0;
}
void findMaxTwo(int *maxTwo, int min, int max) {
	int max1, max2;
	max1 = maxTwo[min], max2 = maxTwo[max];

	int index = 2;
	if (max1 < max2)
		swap(&max1, &max2);
	for (index; index < max; index++) {
		if (maxTwo[index] > max2) {
			max2 = maxTwo[index];
			if (max1 < max2)
				swap(&max1, &max2);
		}
	}
	printf("%d  %d", max1, max2);
}
void swap(int *a, int *b) {
	int *mid;
	*mid = *a;
	*a = *b;
	*b = *mid;
}
