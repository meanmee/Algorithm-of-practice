/*
 ============================================================================
 Name        : BinTreeWithStack.c
 Author      : meank
 Version     : 1
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct _Tree Tree;
struct _Tree {
	int value;
	Tree *left;
	Tree *right;
};

typedef struct _Stack Stack;
struct _Stack {
	void CreatStack(int MAXSIZE);
	void isEampty(Stack *s);
	void push(Stack *S, Tree *t);
	void pop();
};
int main(void) {

	return 0;
}
/*
 * 中序
 */
void InOrderTraversal(Tree *tree) {
	Tree *T = tree;
	Stack *stack;
	Stack *S = CreatStack(100);
	while (T || !isEampty(S)) {
		while (T) {
			push(S, T);
			T->left;
		}
		if (!isEampty(S)) {
			pop();
			printf("%d ", T->value);
			T->right;
		}
	}
}
/*
 * 前序
 */
void InOrderTraversal1(Tree *tree) {
	Tree *T = tree;
	Stack *stack;
	Stack *S = CreatStack(100);
	while (T || !isEampty(S)) {
		while (T) {
			printf("%d ", T->value);
			push(S, T);
			T->left;
		}
		if (!isEampty(S)) {
			pop();
			T->right;
		}
	}
}
/*
 * 后序（不确定行不行）
 */
void InOrderTraversal1(Tree *tree) {
	Tree *T = tree;
	Stack *stack;
	Stack *S = CreatStack(100);
	while (T || !isEampty(S)) {
		while (T) {
			T = T->right;
			while (T) {
				push(S, T);
				T = T->left;
			}
			T = T->left + 1;
		}
		if (!isEampty(S)) {
			pop();
		}
	}
}
