/*
 ============================================================================
 Name        : BinaryFindTest.c
 Author      : meank
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
int binaryFindK();
typedef struct _binaryTest {
	int data[100];
	int length;
} binaryTest;
int main(void) {
	binaryTest b1;
	int i1, h = 0;
	for (i1 = 0; i1 < 100; i1++)
		b1.data[i1] = i1;
	b1.length = 100;
	printf("%d\n", binaryFindK(&b1, 90));
	scanf("%d", &h);
	return 0;
}

int binaryFindK(binaryTest *bTest, int K) {
	int left, right, mid;
	right = bTest->length;
	left = 1;
	while (right >= left) {
		mid = (left + right) / 2;
		if (mid > K) {
			right = mid - 1;
		} else if (mid < K) {
			left = mid + 1;
		} else {
			return mid;
		}
	}
	return -1;
}
