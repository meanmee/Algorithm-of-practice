/*
 ============================================================================
 Name        : zhejiang3-1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

float myFunction(float, float, float, float, float);

float result, rangeArr[2];
float a[4], midw;
int numOfCoef = 4, range = 2, i, j;
int main(void) {

	for (i = 0; i < numOfCoef; i++) {
		scanf("%f", &a[i]);
	}
	for (j = 0; j < range; j++) {
		scanf("%f", &rangeArr[j]);
	}
	midw = (rangeArr[0] + rangeArr[1]) / 2;
	printf("%.2f", myFunction(a[0], a[1], a[2], a[3], midw));

	return 0;
}

float myFunction(float a3, float a2, float a1, float a0, float midd) {

	while ((rangeArr[1] - rangeArr[0]) !=0.01) {
		result = midd *(midd* (midd * a3+ a2) + a1) + a0;
		if (result == 0) {
			return midd;
		} else if (result < 0) {
			rangeArr[1] = midd;
		} else if (result > 0) {
			rangeArr[0] = midd;
		}
		midd = (rangeArr[0] + rangeArr[1]) / 2;
	}
	return midd;
}

