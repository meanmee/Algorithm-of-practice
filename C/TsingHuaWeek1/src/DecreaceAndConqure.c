/*
 ============================================================================
 Name        : TsingHuaWeek1.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
int dac(int a[], int b);
int main(void) {
	int i, k[20];
	for (i = 0; i < 20; i++)
		k[i] = i;
	printf("%d", dac(k, 20));
	return 0;
}
int dac(int a[], int b) {
	return (b < 1) ? 0 : dac(a, b - 1) + a[b - 1];
}
