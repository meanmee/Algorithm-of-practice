/*
 ============================================================================
 Name        : memoization.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

void fibDynamic(int);
int main(void) {
	fibDynamic(4);
	return 0;
}

void fibDynamic(int n) {
	int g = 0, f = 1; //fib(0),fib(1)
	while (0 < n--) {
		g= g + f;
		f= g - f;
	}
	printf("%d", g);
}
