package simpletest;

public class MyGeneric {

	public static void main(String[] args) {
		Test<String> test = new Test<String>();
		test.setX("100");
		test.setY("50");
		System.out.printf("%s %s", test.getX(), test.getY());

	}
}

class Test<T> {
	private T x;
	private T y;

	public T getX() {
		return x;
	}

	public void setX(T x) {
		this.x = x;
	}

	public T getY() {
		return y;
	}

	public void setY(T y) {
		this.y = y;
	}

}
