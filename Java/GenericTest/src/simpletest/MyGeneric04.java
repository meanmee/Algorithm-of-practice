package simpletest;

public class MyGeneric04 {

	public static void main(String[] args) {
		TestOfGenenricFunc tFunc = new TestOfGenenricFunc();
		String s = tFunc.tell("ttt");
		System.out.println(s);
		int i = tFunc.tell(2);
		System.out.println(i);
	}

}

class TestOfGenenricFunc {
	public <T> T tell(T t) {
		return t;
	}
}