package simpletest;

public class MyGenenric05 {

	public static void main(String[] args) {
		String string[] = { "2", "2" };
		H h = new H();
		h.say(string);
		Integer[] i = new Integer[100];
		for (int j = 0; j < 10; j++)
			i[j] = j;
		h.say(i);
	}

}

class H {
	public <T> void say(T array[]) {
		for (int i = 0; i < array.length; i++)
			System.out.println(array[i]);
	}
}
