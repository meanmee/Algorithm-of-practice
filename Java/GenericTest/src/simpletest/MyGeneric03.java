package simpletest;

public class MyGeneric03 {

	public static void main(String[] args) {
		C c = new C("hhh");
		c.say();
	}

}

/*
 * ���ͽӿ�
 */
interface inter<T> {
	public void say();
}

class C implements inter<String> {
	public C(String info) {
		this.s = info;
	}

	private String s;

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	@Override
	public void say() {
		System.out.println(s);
	}

}
