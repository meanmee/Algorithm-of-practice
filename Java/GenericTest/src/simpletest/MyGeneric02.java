package simpletest;

public class MyGeneric02 {

	public static void main(String[] args) {
		Connet<String> con = new Connet<String>();
		con.setX("100");
		tell(con);
	}

	/*
	 * ?为通配符。无论什么类型的泛型都能够传入
	 */
	public static void tell(Connet<?> conn) {
		System.out.println(conn.getX());
	}
}

class Connet<T> {
	private T x;

	public T getX() {
		return x;
	}

	public void setX(T x) {
		this.x = x;
	}

}
