num=0
low=0
high=100
mid=low+high/2
while low<high:
    print("Is your secret number "+str(mid)+"?")
    x=raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly.")
    if x=='h':
        high=mid
        mid=(high+low)/2
    elif x=='l':
        low=mid
        mid=(high+low)/2
    elif x=='c' :
        break
    elif x!='c'or'h'or'l':
        print("Sorry, I did not understand your input.")
print("Game over. Your secret number was: "+str(mid))