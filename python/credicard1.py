#	      Month: 11
#	      Minimum monthly payment: 132.18
#	      Remaining balance: 3225.07
#	      Month: 12
#	      Minimum monthly payment: 129.0
#	      Remaining balance: 3147.67
#	      Total paid: 1775.55
#	      Remaining balance: 3147.67
balance = 4213.0
annualInterestRate = 0.2
monthlyPaymentRate = 0.04

MMPA=0.0
MIR=annualInterestRate/12.0
for i in range(12):
    print 'Month: '+str(i+1)
    MMP=balance*monthlyPaymentRate
    MMPA+=MMP
    print 'Minimum monthly payment: '+str(round(MMP,2))
    balance=balance-MMP+MIR*(balance-MMP)
    print 'Remaining balance: '+str(round(balance,2))
print  'Total paid: '+str(round(MMPA,2))
print  'Remaining balance: '+str(round(balance,2))