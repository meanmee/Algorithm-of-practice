#             Test Case 1
#             balance = 320000
#	      annualInterestRate = 0.2
#
#	      Result Your Code Should Generate:
#	      -------------------
#	      Lowest Payment: 29157.09
balance = 320000
annualInterestRate = 0.2

MIN=balance/12.0
MAX=(balance*(1+(annualInterestRate/12.0))**12)/12.0
MID=(MIN+MAX)/2.0

MIR=annualInterestRate/12.0
cbalance=balance
while MAX-MIN>0.1:
    for i in range(12):
        cbalance=cbalance-MID+MIR*(cbalance-MID)
    if cbalance<0:
        MAX=MID
        MID=(MID+MIN)/2.0
        
    elif cbalance>0:
        MIN=MID
        MID=(MID+MAX)/2.0
    elif cbalance==0:
        break
    cbalance=balance
print str(round(MID,2))
    