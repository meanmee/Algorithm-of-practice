#	      Test Case 1:
#	      balance = 3329
#	      annualInterestRate = 0.2
#
#	      Result Your Code Should Generate:
#	      -------------------
#	      Lowest Payment: 310

balance = 3926
annualInterestRate = 0.2

MIR=annualInterestRate/12.0
MP1=10.0
index=1
cbalance=balance
while index>0:
    for i in range(12):
        cbalance=cbalance-MP1+(cbalance-MP1)*MIR
    if cbalance<=0:
        break
    MP1=MP1+10.0
    cbalance=balance
        
print 'Lowest Payment: '+str(int(MP1))
